package com.rmxburhan.lks_2024.dto

data class Item(
    val product_id: Int,
    val qty: Int,
    val subtotal: Int
)