package com.rmxburhan.lks_2024.data

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.rmxburhan.lks_2024.dto.LoginRequest
import com.rmxburhan.lks_2024.dto.RegisterRequest
import com.rmxburhan.lks_2024.dto.StoreInvoiceRequest
import com.rmxburhan.lks_2024.model.Invoice
import com.rmxburhan.lks_2024.model.Product
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL

class Connect {
    companion object {
        val base_url = "http://103.187.147.96"
        var token = ""
        fun login(request : LoginRequest) : Boolean {
            try{
                val url_link = URL("${base_url}/api/login")

                with(url_link.openConnection() as HttpURLConnection) {
                    requestMethod = "POST"
                    setRequestProperty("Accept", "application/json")
                    setRequestProperty("Content-Type", "application/application/json")

                    val gson = Gson()
                    with(outputStream.bufferedWriter()) {
                        write(gson.toJson(request).toString())
                        flush()
                    }

                    when(responseCode) {
                        in 200..299 -> {
                            val response = JSONObject(inputStream.bufferedReader().readText())
                            token = "Bearer " + response["data"].toString()
    //                        activit.getSharedPreferences("login_session", MODE_PRIVATE)
    //                            .edit()
    //                            .putString("token", token)
    //                            .apply()

                            return  true
                        } else ->  {
                        val response = errorStream.bufferedReader().readText()
                        return false
                    }
                    }
                }
            } catch (ex : Exception) {
                throw ex
            }
        }

        fun register(request : RegisterRequest ) : Boolean {
            try{
                val url_link = URL("${base_url}/api/register")

                with(url_link.openConnection() as HttpURLConnection) {
                    requestMethod = "POST"
                    setRequestProperty("Accept", "application/json")
                    setRequestProperty("Content-Type", "application/application/json")

                    val gson = Gson()
                    with(outputStream.bufferedWriter()) {
                        write(gson.toJson(request).toString())
                        flush()
                    }

                    when(responseCode) {
                        in 200..299 -> {
                            val response = JSONObject(inputStream.bufferedReader().readText())
                            return  true
                        } else ->  {
                            val response = errorStream.bufferedReader().readText()
                         return false
                    }
                    }
                }
            } catch (ex : Exception) {
                throw ex
            }
        }

        fun logout() : Boolean {
            try{
                val url_link = URL("${base_url}/api/logout")

                with(url_link.openConnection() as HttpURLConnection) {
                    requestMethod = "GET"
                    setRequestProperty("Accept", "application/json")
                    setRequestProperty("Authorization", token)

                    when(responseCode) {
                        in 200..299 -> {
                            val response = JSONObject(inputStream.bufferedReader().readText())
                            return  true
                        } else ->  {
                        val response = errorStream.bufferedReader().readText()
                        return false
                    }
                    }
                }
            } catch (ex : Exception) {
                throw ex
            }
        }

        fun getProfile() : com.rmxburhan.lks_2024.model.UserData? {
            try{
                val url_link = URL("${base_url}/api/user")

                with(url_link.openConnection() as HttpURLConnection) {
                    requestMethod = "GET"
                    setRequestProperty("Accept", "application/json")
                    setRequestProperty("Authorization", token)

                    val gson = Gson()

                    when(responseCode) {
                        in 200..299 -> {
                            val response = JSONObject(inputStream.bufferedReader().readText())
                            val profile = gson.fromJson<com.rmxburhan.lks_2024.model.UserData>(response.toString(),  com.rmxburhan.lks_2024.model.UserData::class.java)
                            return  profile
                        } else ->  {
                        val response = errorStream.bufferedReader().readText()
                        return null
                    }
                    }
                }
            } catch (ex : Exception) {
                throw ex
            }
        }

        fun getProducts() : ArrayList<Product> {
            try{
                val url_link = URL("${base_url}/api/products")

                with(url_link.openConnection() as HttpURLConnection) {
                    requestMethod = "GET"
                    setRequestProperty("Accept", "application/json")
                    setRequestProperty("Authorization", token)
                    val gson = Gson()

                    val data : ArrayList<Product> = arrayListOf()
                    when(responseCode) {
                        in 200..299 -> {
                            val response = JSONObject(inputStream.bufferedReader().readText())
                            val token = object : TypeToken<ArrayList<Product>>() {}.type
                            data.addAll(gson.fromJson(response.getJSONArray("data").toString(), token ))
                            return data
                        } else ->  {
                        val response = errorStream.bufferedReader().readText()
                        return data
                    }
                    }
                }
            } catch (ex : Exception) {
                throw ex
            }
        }

        fun getInvoices() : ArrayList<Invoice> {
            try{
                val url_link = URL("${base_url}/api/invoices")

                with(url_link.openConnection() as HttpURLConnection) {
                    requestMethod = "GET"
                    setRequestProperty("Accept", "application/json")
                    setRequestProperty("Authorization", token)
                    val gson = Gson()

                    val data : ArrayList<Invoice> = arrayListOf()
                    when(responseCode) {
                        in 200..299 -> {
                            val response = JSONObject(inputStream.bufferedReader().readText())
                            val token = object : TypeToken<ArrayList<Invoice>>() {}.type
                            data.addAll(gson.fromJson(response.getJSONArray("data").toString(), token ))
                            return data
                        } else ->  {
                        val response = errorStream.bufferedReader().readText()
                        return data
                    }
                    }
                }
            } catch (ex : Exception) {
                throw ex
            }
        }

        fun saveInvoice(request: StoreInvoiceRequest) : Boolean {
            try{
                val url_link = URL("${base_url}/api/store-invoice")

                with(url_link.openConnection() as HttpURLConnection) {
                    requestMethod = "POST"
                    setRequestProperty("Accept", "application/json")
                    setRequestProperty("Content-Type", "application/application/json")
                    setRequestProperty("Authorization", token)

                    val gson = Gson()
                    with(outputStream.bufferedWriter()) {
                        write(gson.toJson(request).toString())
                        flush()
                    }

                    when(responseCode) {
                        in 200..299 -> {
                            val response = JSONObject(inputStream.bufferedReader().readText())
                            return  true
                        } else ->  {
                        val response = errorStream.bufferedReader().readText()
                        return false
                    }
                    }
                }
            } catch (ex : Exception) {
                throw ex
            }
        }
    }
}