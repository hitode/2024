package com.rmxburhan.lks_2024.adapter

import android.icu.text.DecimalFormat
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.rmxburhan.lks_2024.InvoiceActivity
import com.rmxburhan.lks_2024.databinding.ItemInvoiceBinding
import com.rmxburhan.lks_2024.model.Product
import java.util.ArrayList

class ListInvoiceAdapter(val invoiceActivity: InvoiceActivity, val data: ArrayList<Product>) :
    RecyclerView.Adapter<ListInvoiceAdapter.ListInvoiceHolder>() {
    class ListInvoiceHolder(val binding : ItemInvoiceBinding) : ViewHolder(binding.root) {

    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ListInvoiceAdapter.ListInvoiceHolder {
        return ListInvoiceHolder(
            ItemInvoiceBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ListInvoiceAdapter.ListInvoiceHolder, position: Int) {
        val data = data.get(position)
        with(holder) {
            binding.txtNamaMenu.setText(data.name)
            binding.txtQty.setText(data.qty.toString())
            binding.txtNamaMenu.setText(data.name)
            binding.txtSubTotal.setText("Rp. " + DecimalFormat("#,###").format(data.qty * data.price))
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

}
