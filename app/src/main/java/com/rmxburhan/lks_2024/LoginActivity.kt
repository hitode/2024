package com.rmxburhan.lks_2024

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.rmxburhan.lks_2024.data.Connect
import com.rmxburhan.lks_2024.databinding.ActivityLoginBinding
import com.rmxburhan.lks_2024.dto.LoginRequest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.thread

class LoginActivity : AppCompatActivity() {
    private lateinit var binding : ActivityLoginBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnLogin.setOnClickListener {
            val username = binding.edtUsername.text.toString()
            val password = binding.edtPassword.text.toString()
            if (username.isNullOrEmpty() || password.isNullOrEmpty()) {
                Toast.makeText(this, "Username dan password tidak boleh kosong", Toast.LENGTH_SHORT)
                    .show()
                return@setOnClickListener
            }
            lifecycleScope.launch(Dispatchers.IO) {

            try {
                val status = Connect.login(LoginRequest(password, username))

                lifecycleScope.launch(Dispatchers.Main) {
                    if (status) {
                        binding.edtUsername.setText("")
                        binding.edtPassword.setText("")
                        startActivity(Intent(this@LoginActivity, MenuActivity::class.java))
                    } else {
                        Toast.makeText(this@LoginActivity, "Login gagal", Toast.LENGTH_SHORT).show()
                    }
                }
            } catch (ex : Exception) {
                Log.d("login-err", ex.toString())
                Toast.makeText(this@LoginActivity, ex.message, Toast.LENGTH_SHORT).show()
            }
            }

        }

        binding.btnRegister.setOnClickListener {
            binding.edtUsername.setText("")
            binding.edtPassword.setText("")

            startActivity(Intent(this@LoginActivity, RegisterActivity::class.java))
        }
    }
}