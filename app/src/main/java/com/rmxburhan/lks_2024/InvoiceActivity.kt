package com.rmxburhan.lks_2024

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.icu.text.DecimalFormat
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.bumptech.glide.gifdecoder.GifDecoder.BitmapProvider
import com.rmxburhan.lks_2024.adapter.ListInvoiceAdapter
import com.rmxburhan.lks_2024.data.Keranjang
import com.rmxburhan.lks_2024.databinding.ActivityInvoiceBinding
import java.io.File
import java.time.LocalDateTime

class InvoiceActivity : AppCompatActivity() {
    private lateinit var binding : ActivityInvoiceBinding
    private lateinit var adapter : ListInvoiceAdapter
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityInvoiceBinding.inflate(layoutInflater)
        setContentView(binding.root)

        adapter = ListInvoiceAdapter(this, Keranjang.listInvoice)
        binding.listInvoice.adapter = adapter

        binding.txtTotalHarga.setText("Total : Rp. " + DecimalFormat("#,###").format(Keranjang.listInvoice.sumOf { it.qty * it.price }))

        binding.btnSave.setOnClickListener {
            try {
                val file = bitmapToFile(binding.invoicePng, LocalDateTime.now().toString())
                if (file == null) {
                    Toast.makeText(this, "Gagal menyimpan invoice", Toast.LENGTH_SHORT).show()
                    return@setOnClickListener
                }
                startActivity(Intent().apply {
                    action = Intent.ACTION_VIEW
                    addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    setDataAndType(FileProvider.getUriForFile(this@InvoiceActivity, "lks-2024.provider", file), "image/*")
                })
            } catch (ex : Exception) {
            }
        }

        binding.btnShare.setOnClickListener {
            val file = bitmapToFile(binding.invoicePng, LocalDateTime.now().toString())
            if (file == null) {
                Toast.makeText(this, "Gagal menyimpan invoice", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            startActivity(Intent().apply {
                action = Intent.ACTION_SEND
                addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                putExtra(Intent.EXTRA_STREAM, FileProvider.getUriForFile(this@InvoiceActivity, "lks-2024.provider", file))
                type = "image/*"
            })
        }

        binding.btnSelesai.setOnClickListener {
            finish()
        }
    }

    fun bitmapToFile(view : View, filenName : String) : File? {
        val bitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        view.draw(canvas)
        try {
            val file = File(getExternalFilesDir(Environment.getExternalStorageState().toString()), filenName + ".jpeg")
            file.createNewFile()
            file.outputStream().run {
                bitmap.compress(Bitmap.CompressFormat.JPEG, 85, this)
                flush()
                close()
            }
            return file
        } catch (ex : Exception) {
            Log.d("save-err", ex.toString())
            return null
        }
    }

}