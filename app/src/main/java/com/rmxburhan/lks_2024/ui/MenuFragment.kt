package com.rmxburhan.lks_2024.ui

import android.content.ClipData.Item
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.SearchView.OnQueryTextListener
import androidx.lifecycle.lifecycleScope
import com.rmxburhan.lks_2024.InvoiceActivity
import com.rmxburhan.lks_2024.R
import com.rmxburhan.lks_2024.adapter.ItemMenuAdapter
import com.rmxburhan.lks_2024.data.Connect
import com.rmxburhan.lks_2024.data.Keranjang
import com.rmxburhan.lks_2024.databinding.FragmentMenuBinding
import com.rmxburhan.lks_2024.dto.StoreInvoiceRequest
import com.rmxburhan.lks_2024.model.Product
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.text.DecimalFormat
import kotlin.concurrent.thread

class MenuFragment : Fragment() {
    private lateinit var binding : FragmentMenuBinding
    private lateinit var adapter : ItemMenuAdapter

    var menu : ArrayList<Product> = arrayListOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMenuBinding.inflate(layoutInflater)

        adapter = ItemMenuAdapter(this, arrayListOf<Product>(), binding.txtTotal)
        binding.listMenu.adapter = adapter

        binding.searchView.setOnQueryTextListener(object : OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                adapter.data.clear()
                adapter.data.addAll(menu.filter { it.name.lowercase().startsWith(newText.lowercase().toString()) })
                adapter.notifyDataSetChanged()
                return true
            }

        })

        binding.btnBayar.setOnClickListener {
            saveTransaksi()
        }

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        setupMenu()
    }

    fun saveTransaksi() {
            if (Keranjang.listKeranjang.filter{it.qty > 0}.size == 0) {
                Toast.makeText(requireActivity(), "Tidak ada barang yang dipilih", Toast.LENGTH_SHORT).show()
                return
            }

        val items : ArrayList<com.rmxburhan.lks_2024.dto.Item> = arrayListOf()
        Keranjang.listKeranjang.forEach {
            items.add(com.rmxburhan.lks_2024.dto.Item(
                it.id,
                it.qty,
                it.qty * it.price
            ))
        }

        val request = StoreInvoiceRequest(items, Keranjang.listKeranjang.sumOf { it.qty * it.price }, Keranjang.listKeranjang.sumOf { it.qty })
        lifecycleScope.launch(Dispatchers.IO) {
            try {
                val status = Connect.saveInvoice(request )
                lifecycleScope.launch(Dispatchers.Main) {
                    if (status) {
                        Toast.makeText(
                            requireActivity(),
                            "Transaksi berhasil disimpan",
                            Toast.LENGTH_SHORT
                        ).show()
                        Keranjang.listInvoice.clear()
                        Keranjang.listInvoice.addAll(Keranjang.listKeranjang)
                        Keranjang.listKeranjang.clear()
                        startActivity(Intent(requireActivity(), InvoiceActivity::class.java))
                    } else {
                        Toast.makeText(requireActivity(), "Gagal menyimpan transaksi", Toast.LENGTH_SHORT).show()
                    }
                }
            } catch (ex : Exception) {
                Log.d("err-save", ex.toString())
            }
        }
    }


    fun setupMenu() {
        lifecycleScope.launch(Dispatchers.IO) {
            try {

                val data = Connect.getProducts()

                if (data.size == 0) {
                    lifecycleScope.launch(Dispatchers.Main){
                        Toast.makeText(requireActivity(), "Tidak ada data untuk ditampilkan", Toast.LENGTH_SHORT).show()
                    }
                }

                menu.clear()
                menu.addAll(data)
                adapter.data.clear()
                adapter.data.addAll(data)
                binding.txtTotal.setText("Rp. "  + DecimalFormat("#,###").format(Keranjang.listKeranjang.sumOf { it.qty * it.price }))
                lifecycleScope.launch(Dispatchers.Main) {
                    adapter.notifyDataSetChanged()
                }

            } catch (ex : Exception) {
                Log.d("err-menu", ex.toString())
            }
        }
    }

}