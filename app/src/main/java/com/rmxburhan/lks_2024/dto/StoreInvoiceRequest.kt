package com.rmxburhan.lks_2024.dto

data class StoreInvoiceRequest(
    val items: List<Item>,
    val price_total: Int,
    val qty_total: Int
)