package com.rmxburhan.lks_2024.model

data class UserData(
    val address: String,
    val created_at: String,
    val id: Int,
    val image: String,
    val is_admin: Int,
    val name: String,
    val updated_at: String,
    val username: String
)