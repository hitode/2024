package com.rmxburhan.lks_2024.dto

data class RegisterRequest(
    val address: String,
    val name: String,
    val password: String,
    val password_confirmation: String,
    val username: String
)