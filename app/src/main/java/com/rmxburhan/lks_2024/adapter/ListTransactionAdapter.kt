package com.rmxburhan.lks_2024.adapter

import android.icu.text.DecimalFormat
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.rmxburhan.lks_2024.databinding.ItemInvoicesBinding
import com.rmxburhan.lks_2024.model.Invoice
import com.rmxburhan.lks_2024.ui.InvoiceFragment
import java.text.SimpleDateFormat
import java.util.Locale

class ListTransactionAdapter(val fragment: InvoiceFragment, val data : ArrayList<Invoice>) : RecyclerView.Adapter<ListTransactionAdapter.ListTransactionHolder>() {
    class ListTransactionHolder(val binding : ItemInvoicesBinding) : ViewHolder(binding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListTransactionHolder {
        return ListTransactionHolder(
            ItemInvoicesBinding.inflate(LayoutInflater.from(parent.context),parent, false)
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ListTransactionHolder, position: Int) {
        val item = data.get(position)

        with(holder) {
            binding.txtNoTransaksi.setText(item.invoice_num)
            binding.txtTotalHarga.setText("Rp. " + DecimalFormat("#,###").format(item.price_total))
            val inputformat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            val outputFormat = SimpleDateFormat("EEEE, dd MMMM yyyy, HH:mm:ss", Locale.getDefault())
            val date = inputformat.parse(item.created_at)
            binding.txtTglTransaksi.setText("Tanggal : " + outputFormat.format(date))
        }
    }

}
