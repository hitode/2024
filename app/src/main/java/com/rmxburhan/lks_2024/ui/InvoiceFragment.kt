package com.rmxburhan.lks_2024.ui

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import com.rmxburhan.lks_2024.adapter.ListTransactionAdapter
import com.rmxburhan.lks_2024.data.Connect
import com.rmxburhan.lks_2024.databinding.FragmentInvoiceBinding
import com.rmxburhan.lks_2024.model.Invoice
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.thread

class InvoiceFragment : Fragment() {
    private lateinit var binding : FragmentInvoiceBinding
    private lateinit var adapter : ListTransactionAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentInvoiceBinding.inflate(layoutInflater)

        adapter = ListTransactionAdapter(this, arrayListOf<Invoice>())
        binding.listInvoice.adapter = adapter
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        setupInvoice()
    }

    fun setupInvoice() {
        lifecycleScope.launch(Dispatchers.IO) {
            try {
                val data = Connect.getInvoices()
                lifecycleScope.launch(Dispatchers.Main) {
                    if (data.size == 0) {
                        Toast.makeText(requireActivity(), "Tidak ada data untuk ditampilkan", Toast.LENGTH_SHORT)
                            .show()
                    }

                    adapter.data.clear()
                    adapter.data.addAll(data)
                    adapter.notifyDataSetChanged()
                }
            } catch (ex : Exception) {
                Log.d("err-invoices", ex.toString())
            }
        }
    }
}