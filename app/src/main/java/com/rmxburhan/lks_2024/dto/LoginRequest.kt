package com.rmxburhan.lks_2024.dto

data class LoginRequest(
    val password: String,
    val username: String
)