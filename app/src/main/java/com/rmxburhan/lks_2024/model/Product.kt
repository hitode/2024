package com.rmxburhan.lks_2024.model

data class Product(
    val created_at: String,
    val id: Int,
    val image: String,
    val name: String,
    val price: Int,
    val updated_at: String,
    var qty : Int = 0
)