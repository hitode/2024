package com.rmxburhan.lks_2024

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.lifecycle.lifecycleScope
import com.rmxburhan.lks_2024.data.Connect
import com.rmxburhan.lks_2024.databinding.ActivityRegisterBinding
import com.rmxburhan.lks_2024.dto.RegisterRequest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.thread

class RegisterActivity : AppCompatActivity() {
    private lateinit var binding : ActivityRegisterBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnRegister.setOnClickListener {
            val name = binding.edtNama.text.toString()
            val address = binding.edtAlamat.text.toString()
            val username = binding.edtUsername.text.toString()
            val password = binding.edtPassword.text.toString()
            val password_confirm = binding.edtPasswordConfirm.text.toString()
            if (name.isNullOrEmpty() || address.isNullOrEmpty() || username.isNullOrEmpty() || password_confirm.isNullOrEmpty() || password.isNullOrEmpty()) {
                Toast.makeText(this, "Lengkapi data diatas", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (password_confirm != password) {
                Toast.makeText(this, "Password tidak sama", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            lifecycleScope.launch(Dispatchers.IO) {

            try {
                val status = Connect.register(RegisterRequest(address, name, password, password_confirm, username))

                lifecycleScope.launch(Dispatchers.Main) {
                    if (status) {
                        Toast.makeText(this@RegisterActivity, "Registrasi berhasil", Toast.LENGTH_SHORT).show()
                        finish()
                    } else {
                        Toast.makeText(this@RegisterActivity, "Registrasi gagal", Toast.LENGTH_SHORT).show()
                    }
                }
            } catch (ex: Exception) {
                Log.d("register-err", ex.toString())
                Toast.makeText(this@RegisterActivity, ex.message, Toast.LENGTH_SHORT).show()
            }
            }

        }

        binding.btnSudahPunyaAkun.setOnClickListener {
            finish()
        }
    }
}