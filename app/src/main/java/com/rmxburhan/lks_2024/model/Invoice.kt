package com.rmxburhan.lks_2024.model

data class Invoice(
    val created_at: String,
    val id: Int,
    val invoice_num: String,
    val price_total: Int,
    val qty_total: Int,
    val updated_at: String,
    val user_id: Int
)