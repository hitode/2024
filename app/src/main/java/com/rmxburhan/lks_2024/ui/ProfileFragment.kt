package com.rmxburhan.lks_2024.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.rmxburhan.lks_2024.R
import com.rmxburhan.lks_2024.data.Connect
import com.rmxburhan.lks_2024.databinding.FragmentProfileBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.thread

class ProfileFragment : Fragment() {

    private lateinit var binding : FragmentProfileBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentProfileBinding.inflate(layoutInflater)


        binding.btnLogout.setOnClickListener {
            logout()
        }
        return binding.root
    }

    fun logout() {
        thread {
            if (Connect.logout()) {
                requireActivity().runOnUiThread {
                    requireActivity().finish()
                }
            } else {
                requireActivity().runOnUiThread {
                    Toast.makeText(requireActivity(), "Silahkan login kembali", Toast.LENGTH_SHORT).show()
                    requireActivity().finish()
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        setupProfile()
    }

    fun setupProfile() {
        lifecycleScope.launch(Dispatchers.IO) {
            val data = Connect.getProfile()


            lifecycleScope.launch(Dispatchers.Main){
                if (data != null) {

                    binding.txtAlamat.setText(data.address)
                    binding.txtNama.setText(data.name)
                    binding.txtTelpon.setText(data.username)
                    try {
                        Glide.with(this@ProfileFragment)
                            .load(data.image)
                            .timeout(3000)
                            .into(binding.imgProfile)
                    } catch (ex : Exception) {

                    }

                } else {
                    Toast.makeText(requireActivity(), "Gagal mendownload profile", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }


}