package com.rmxburhan.lks_2024.adapter

import android.icu.text.DecimalFormat
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.Glide
import com.rmxburhan.lks_2024.data.Keranjang
import com.rmxburhan.lks_2024.databinding.ItemMenuBinding
import com.rmxburhan.lks_2024.model.Product
import com.rmxburhan.lks_2024.ui.MenuFragment
import java.util.ArrayList

class ItemMenuAdapter(
    val menuFragment: MenuFragment,
    val data: ArrayList<Product>,
    val txtTotal: TextView
) : RecyclerView.Adapter<ItemMenuAdapter.ItemMenuHolder>() {
    class ItemMenuHolder(val binding : ItemMenuBinding) : ViewHolder(binding.root) {

    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ItemMenuAdapter.ItemMenuHolder {
        return ItemMenuHolder(
            ItemMenuBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ItemMenuAdapter.ItemMenuHolder, position: Int) {
        val item = data.get(position)
        val realItem = menuFragment.menu.find { it.id == item.id }
        val keranjangExist = Keranjang.listKeranjang.find { it.id  == item.id }
        with(holder) {
            binding.txtNamaMenu.setText(item.name)
            binding.txtPrice.setText("Rp. " + DecimalFormat("#,###").format(item.price))
            if (keranjangExist != null) {
                binding.txtQty.setText(keranjangExist.qty.toString())
            } else {
                binding.txtQty.setText(realItem?.qty.toString())
            }

            try {
                Glide.with(binding.root)
                    .load(item.image)
                    .timeout(3000)
                    .into(binding.imgMenu2)
            } catch (ex : Exception) {

            }

            realItem?.let {
                binding.btnAdd.setOnClickListener {
                    realItem.qty++
                    binding.txtQty.setText(realItem.qty.toString())
                }

                binding.btnRemoveQty.setOnClickListener {
                    if (realItem.qty > 0) {
                        realItem.qty--
                        binding.txtQty.setText(realItem.qty.toString())
                    }
                }

                binding.btnCart.setOnClickListener {
                    if(keranjangExist != null) {
                        keranjangExist.qty = realItem.qty
                        val total = Keranjang.listKeranjang.sumOf { it.qty * it.price }
                        txtTotal.setText("Rp. " + DecimalFormat("#,###").format(total))
                    } else {
                        Keranjang.listKeranjang.add(realItem)
                        val total = Keranjang.listKeranjang.sumOf { it.qty * it.price }
                        txtTotal.setText("Rp. " + DecimalFormat("#,###").format(total))
                    }
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

}
